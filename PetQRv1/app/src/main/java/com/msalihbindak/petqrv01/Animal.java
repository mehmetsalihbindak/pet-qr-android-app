package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by salih on 11.04.2018.
 */

public class Animal extends ArrayAdapter<String> {


    private final ArrayList<Bitmap> animalImage;
    private final ArrayList<String> animalID;
    private final ArrayList<String> animalName;
    private final ArrayList<String> animalBasicInf;
    private final ArrayList<String> animalVaccines;
    private final ArrayList<String> animalAlergies;

    private final Activity context;

    public Animal(ArrayList<Bitmap> animalImage,
                  ArrayList<String> animalID,
                  ArrayList<String> animalName,
                  ArrayList<String> animalBasicInf,
                  ArrayList<String> animalVaccines,
                  ArrayList<String> animalAlergies,
                  Activity context) {
        super(context,R.layout.animalprofile_view,animalID);

        this.animalImage = animalImage;
        this.animalID = animalID;
        this.animalName = animalName;
        this.animalBasicInf = animalBasicInf;
        this.animalVaccines = animalVaccines;
        this.animalAlergies = animalAlergies;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = context.getLayoutInflater();
        View animalProfileView = layoutInflater.inflate(R.layout.animalprofile_view,null,true);
        ImageView aImage = animalProfileView.findViewById(R.id.animalImage);
        TextView  aName  = animalProfileView.findViewById(R.id.animalName);
        TextView  aID    = animalProfileView.findViewById(R.id.animalID);
        TextView aBasicInf = animalProfileView.findViewById(R.id.animalBasicInf);
        TextView aVaccines = animalProfileView.findViewById(R.id.animalVaccines);
        TextView aAlergies = animalProfileView.findViewById(R.id.animalAlergies);

        aImage.setImageBitmap(animalImage.get(position));
        aName.setText(animalName.get(position));
        aID.setText(animalID.get(position));
        aBasicInf.setText(animalBasicInf.get(position));
        aVaccines.setText(animalVaccines.get(position));
        aAlergies.setText(animalAlergies.get(position));



        return animalProfileView;
    }
}
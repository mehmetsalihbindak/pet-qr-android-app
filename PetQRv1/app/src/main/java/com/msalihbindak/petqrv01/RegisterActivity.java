package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
*/

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class RegisterActivity extends AppCompatActivity {
    EditText nameText;
    EditText usernameText;
    EditText emailText;
    EditText passwordText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nameText = findViewById(R.id.nameText);
        usernameText = findViewById(R.id.usernameText);
        emailText = findViewById(R.id.emailText);
        passwordText = findViewById(R.id.passwordText);

    }



    public void register(View view){

        //User Register
        ParseUser user = new ParseUser();
        //https://stackoverflow.com/questions/26435079/parse-com-with-parseuser-how-can-i-save-data-in-a-column-i-created-in-parse-fr
        user.put("name",nameText.getText().toString());
        user.setUsername(usernameText.getText().toString());
        user.setEmail(emailText.getText().toString());
        user.setPassword(passwordText.getText().toString());

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {

                if(e != null){
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }else{
                    //alert.setTitle("Save");
                    Toast.makeText(getApplicationContext(),"You Signed Up successfuly!!!",Toast.LENGTH_LONG).show();
                    Intent login = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(login);
                }

            }
        });




    }

}

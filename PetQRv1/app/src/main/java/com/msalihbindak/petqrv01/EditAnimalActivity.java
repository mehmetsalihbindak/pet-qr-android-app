package com.msalihbindak.petqrv01;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

public class EditAnimalActivity extends AppCompatActivity {

    private int CHOOSE_IMAGE_REQUEST_CODE = 2;
    private int PERMISSION_REQUEST_CODE = 1;

    ImageView animalImage;
    Bitmap choosenAnimalImage;
    EditText animalName;
    EditText animalBasicInf;
    EditText animalVaccines;
    EditText animalAlergies;

    String animalID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_animal);
        animalImage = findViewById(R.id.animalPic);
        animalName = findViewById(R.id.animalName);
        animalBasicInf = findViewById(R.id.animalBasic);
        animalVaccines = findViewById(R.id.animalVaccines);
        animalAlergies = findViewById(R.id.animalAlergies);

        Intent hayvan = getIntent();

        String oldName = hayvan.getStringExtra("name");
        String oldBasicInf = hayvan.getStringExtra("basicInf");
        String oldVaccines = hayvan.getStringExtra("vaccines");
        String oldAlergies = hayvan.getStringExtra("alergies");


        byte[] byteArray = getIntent().getByteArrayExtra("image");
        Bitmap oldBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        animalImage.setImageBitmap(oldBitmap);


        animalID = hayvan.getStringExtra("id");
        // For Debug //System.out.println(animalID);
        animalName.setText(oldName);
        animalBasicInf.setText(oldBasicInf);
        animalVaccines.setText(oldVaccines);
        animalAlergies.setText(oldAlergies);
        animalImage.setImageBitmap(oldBitmap);


    }


    @TargetApi(Build.VERSION_CODES.M)
    public void chooseImage(View view){
        // Check permissions if ok go to take picture
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
            Toast.makeText(getApplication(),"You will go to take picture...",Toast.LENGTH_LONG).show();
        }else{
            // Go to take request...
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSION_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Eleman izin vermiş resmi çekmeye gidiyorum.
                Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
                // Choose Image içindeki kodun aynısı çünkü orada da izin varsa yapacağım şeyleri şimdi izin aldıktan sonra yapıyorum.
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHOOSE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            //izin isteyip OK cevabı döndüğünü ve resmi galeriden çekebildiğimi teyit ettim.
            Uri selectedImage = data.getData();
            try{
                choosenAnimalImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                animalImage.setImageBitmap(choosenAnimalImage);
            }catch (Exception e){
                // e.printStackTrace(); // For debugging
                Toast.makeText(getApplication(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }




    public void updateAnimal(View view){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Animals");

        query.getInBackground(animalID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                // Do not remove controls it throws null pointer exception.
                if(choosenAnimalImage != null){
                    ByteArrayOutputStream byteArrayOfAnimalImage = new ByteArrayOutputStream();
                    choosenAnimalImage.compress(Bitmap.CompressFormat.PNG,50,byteArrayOfAnimalImage);
                    byte[] imageBytes = byteArrayOfAnimalImage.toByteArray();
                    ParseFile updatedAnimalImage = new ParseFile("animal.png",imageBytes);
                    object.put("image",updatedAnimalImage);
                }

                // This if statements improvements the speed
                if(animalName != null)
                    object.put("name",animalName.getText().toString());

                if(animalBasicInf != null)
                    object.put("basicInf",animalBasicInf.getText().toString());

                if(animalAlergies != null)
                    object.put("alergies",animalAlergies.getText().toString());

                if(animalVaccines != null)
                    object.put("vaccines",animalVaccines.getText().toString());

                object.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null){
                            Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(),"Successfully Updated!",Toast.LENGTH_SHORT).show();
                            Intent dash = new Intent(getApplicationContext(),DashboardActivity.class);
                            startActivity(dash);
                        }
                    }
                });
            }
        });


    }

    public void deleteAnimal(View view){
        //https://stackoverflow.com/questions/28564162/delete-a-row-from-parse-table/50124719#50124719

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Animals");
        query.getInBackground(animalID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {

                try {
                    object.delete();
                    object.saveInBackground();

                    Intent dashboard = new Intent(getApplicationContext(),DashboardActivity.class);
                    Toast.makeText(getApplicationContext(), "Animal Deleted Successuly.", Toast.LENGTH_SHORT).show();
                    startActivity(dashboard);

                } catch (ParseException e1) {
                    e1.printStackTrace();
                }


            }
        });



    }
}

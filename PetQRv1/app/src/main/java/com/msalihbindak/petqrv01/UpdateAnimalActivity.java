package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

public class UpdateAnimalActivity extends AppCompatActivity {
    private int CHOOSE_IMAGE_REQUEST_CODE = 2;
    private int PERMISSION_REQUEST_CODE = 1;
    ImageView animalImage;
    Bitmap choosenAnimalImage;
    EditText animalName;
    EditText animalBasicInf;
    EditText animalVaccines;
    EditText animalAlergies;
    ParseUser petOwner;
    String idFromEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_animal);
        animalImage = findViewById(R.id.animalPic);
        animalName = findViewById(R.id.animalName);
        animalBasicInf = findViewById(R.id.animalBasic);
        animalVaccines = findViewById(R.id.animalVaccines);
        animalAlergies = findViewById(R.id.animalAlergies);
        petOwner = ParseUser.getCurrentUser();


        bilgileriCek();





    }


     public void bilgileriCek(){

         Intent hayvan = getIntent();
         Bitmap bitmapFromEdit = (Bitmap) hayvan.getParcelableExtra("BitmapImage");
         String nameFromEdit = hayvan.getStringExtra("name");
         String basicInfFromEdit = hayvan.getStringExtra("basicInf");
         String vaccinesFromEdit = hayvan.getStringExtra("vaccines");
         String alergiesFromEdit = hayvan.getStringExtra("alergies");
         idFromEdit = hayvan.getStringExtra("id");


         animalImage.setImageBitmap(bitmapFromEdit);
         animalName.setText(nameFromEdit);
         animalBasicInf.setText(basicInfFromEdit);
         animalVaccines.setText(vaccinesFromEdit);
         animalAlergies.setText(alergiesFromEdit);


     }



    @TargetApi(Build.VERSION_CODES.M)
    public void chooseImage(View view){
        // Check permissions if ok go to take picture
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
            Toast.makeText(getApplication(),"You will go to take picture...",Toast.LENGTH_LONG).show();
        }else{
            // Go to take request...
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSION_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Eleman izin vermiş resmi çekmeye gidiyorum.
                Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
                // Choose Image içindeki kodun aynısı çünkü orada da izin varsa yapacağım şeyleri şimdi izin aldıktan sonra yapıyorum.
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHOOSE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            //izin isteyip OK cevabı döndüğünü ve resmi galeriden çekebildiğimi teyit ettim.
            Uri selectedImage = data.getData();
            try{
                choosenAnimalImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                animalImage.setImageBitmap(choosenAnimalImage);
            }catch (Exception e){
                // e.printStackTrace(); // For debugging
                Toast.makeText(getApplication(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void updateAnimal(View view){
        // Check name, and basic information is full


            final String aName = animalName.getText().toString();
            final String aBasicInf = animalBasicInf.getText().toString();
            final String aVaccines = animalVaccines.getText().toString();
            final String aAlergies = animalAlergies.getText().toString();

            // I tried the aName == null  it does not work. Do not try!


            // IMAGE CONVERTS TO PARSE OBJECT START
            ByteArrayOutputStream byteArrayOfAnimalImage = new ByteArrayOutputStream();
            choosenAnimalImage.compress(Bitmap.CompressFormat.PNG,50,byteArrayOfAnimalImage);
            byte[] imageBytes = byteArrayOfAnimalImage.toByteArray();
            final ParseFile parseAnimalImage = new ParseFile("animal.png",imageBytes);
            // END

        if(aName.isEmpty() || aBasicInf.isEmpty()){
            Toast.makeText(getApplication(),"Please fill at least Name and Basic Information",Toast.LENGTH_SHORT).show();
        }else {

            //https://stackoverflow.com/questions/13251955/how-can-i-update-current-object-in-parse-com-with-javascript

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Animals");

            query.getInBackground(idFromEdit, new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    object.put("name",aName); // Değiştrimek istediklerini gönder
                    object.put("basicInf",aBasicInf);
                    object.put("vaccines",aVaccines);
                    object.put("alergies",aAlergies);
                    object.put("image",parseAnimalImage);
                    object.saveInBackground();
                }
            });



        }

            }

    }




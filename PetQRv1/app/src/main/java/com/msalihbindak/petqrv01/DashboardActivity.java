package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.LogOutCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {
    TextView welcome;
    TextView animalCount;

    ArrayList<Bitmap> animalImageFromParse;
    ArrayList<String> animalNameFromParse;
    ArrayList<String> animalIDFromParse;
    ArrayList<String> animalBasicInfFromParse;
    ArrayList<String> animalVaccinesFromParse;
    ArrayList<String> animalAlergiesFromParse;

    ListView myAnimals;
    Animal animalAdapter;

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.dashboard_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }
*/

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
                if(item.getItemId() == R.id.add_animal){
                    Intent addAnimal = new Intent(getApplicationContext(),AddAnimalActivity.class);
                    startActivity(addAnimal);
        }
        if(item.getItemId() == R.id.logout){
            ParseUser.logOutInBackground(new LogOutCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null){
                        Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                    }else{
                        ParseUser.logOut();
                        Toast.makeText(getApplicationContext(),"We hope see you again!",Toast.LENGTH_LONG).show();
                        Intent home = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(home);
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }
    */




    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.add_animal){
                    Intent addAnimal = new Intent(getApplicationContext(),AddAnimalActivity.class);
                    startActivity(addAnimal);
                }
                if(item.getItemId() == R.id.logout){
                    ParseUser.logOutInBackground(new LogOutCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e != null){
                                Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                            }else{
                                ParseUser.logOut();
                                Toast.makeText(getApplicationContext(),"We hope see you again!",Toast.LENGTH_LONG).show();
                                Intent home = new Intent(getApplicationContext(),MainActivity.class);
                                MainActivity.isLoggedIn = false;
                                startActivity(home);
                            }
                        }
                    });
                }

                return false;
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);




        animalCount  =findViewById(R.id.animalCount);
        System.out.println("ParseUser.getCurrentUser.toString->>> "+ ParseUser.getCurrentUser().toString());
        System.out.println("ParseUser.getCurrentUser.getUsername->>> "+ ParseUser.getCurrentUser().getUsername());
        System.out.println(" ParseUser.getCurrentUser().getSessionToken() ->>>> "+ ParseUser.getCurrentUser().getSessionToken());

        welcome = findViewById(R.id.welcomeText);
        animalImageFromParse = new ArrayList<Bitmap>();
        animalNameFromParse = new ArrayList<String>();
        animalIDFromParse = new ArrayList<String>();
        animalBasicInfFromParse = new ArrayList<String>();
        animalVaccinesFromParse = new ArrayList<String>();
        animalAlergiesFromParse = new ArrayList<String>();

        animalAdapter = new Animal(animalImageFromParse,
                                    animalIDFromParse,
                                    animalNameFromParse,
                                    animalBasicInfFromParse,
                                    animalVaccinesFromParse,
                                    animalAlergiesFromParse,
                        this);

        myAnimals = findViewById(R.id.myAnimalList);

        myAnimals.setAdapter(animalAdapter);

        myAnimals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // For Debug //Toast.makeText(getApplicationContext(),"Clicked" + animalNameFromParse.get(position),Toast.LENGTH_SHORT).show();

                // Edit Animal Information
                Intent editAnimal = new Intent(getApplicationContext(),AnimalPageActivity.class);
                editAnimal.putExtra("objectID",animalIDFromParse.get(position));
                editAnimal.putExtra("auth",true); // Because it is your animal
                startActivity(editAnimal);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);



            }
        });
        String welcomeT = "Welcome "+ParseUser.getCurrentUser().getUsername();
        welcome.setText(welcomeT);
        getMyAnimals();

    }

    public void getMyAnimals(){

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Animals");

            query.whereEqualTo("ownerName",ParseUser.getCurrentUser().getUsername());


        //where, kişi sadece kendisine ait elemanları görecek.
        //query.whereEqualTo("ownerName",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null){
                    if(objects.size()>0){
                        for(final ParseObject object : objects){
                                ParseFile animalImage = (ParseFile) object.get("image");
                                animalImage.getDataInBackground(new GetDataCallback() {
                                    @Override
                                    public void done(byte[] data, ParseException e) {
                                        if(e == null && data != null){
                                            Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                                            animalImageFromParse.add(bitmap);

                                            animalNameFromParse.add(object.getString("name"));
                                            animalIDFromParse.add(object.getObjectId());
                                            animalBasicInfFromParse.add(object.getString("basicInf"));
                                            animalVaccinesFromParse.add(object.getString("vaccines"));
                                            animalAlergiesFromParse.add(object.getString("alergies"));
                                            //Debugging//System.out.println("object.getString(ownerName)->>>  "+ object.getString("ownerName"));
                                            animalAdapter.notifyDataSetChanged();

                                        }else{
                                            Toast.makeText(getApplicationContext(),"There is an error but I don't know why",Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });


                        }
                    }

                    String count = "Currently Animal You Have is "+ objects.size();
                    animalCount.setText(count);
                }else{
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }

            }
        });

    }





}

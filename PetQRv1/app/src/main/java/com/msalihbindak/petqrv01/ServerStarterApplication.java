package com.msalihbindak.petqrv01;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

/**
 * Created by salih on 28.03.2018.
 */
// http://parseplatform.org/
public class ServerStarterApplication extends Application {
// This class is only for use the Parse Server Application
    @Override
    public void onCreate() {
        super.onCreate();
        //Enable Local Datastore
        Parse.enableLocalDatastore(this);

        //Initialization code
        Parse.initialize(this);

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        //Optionally enable public read access
        defaultACL.setPublicWriteAccess(true);
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL,true);
    }
}

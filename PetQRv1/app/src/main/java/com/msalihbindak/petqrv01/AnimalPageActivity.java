package com.msalihbindak.petqrv01;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.ByteArrayOutputStream;
import java.util.List;
public class AnimalPageActivity extends AppCompatActivity {
    ImageView editButton;
    TextView animalName;
    TextView animalID;
    ImageView animalPic;
    TextView animalBasicInf;
    TextView animalVaccines;
    TextView animalAlergies;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_page);

        editButton = findViewById(R.id.editAnimal);
        animalName = findViewById(R.id.animalNameFromQR);
        animalID = findViewById(R.id.animalIDFromParse);
        animalPic = findViewById(R.id.animalPicFromParse);
        animalBasicInf = findViewById(R.id.basicInfFromParse);
        animalVaccines = findViewById(R.id.vaccinesFromParse);
        animalAlergies = findViewById(R.id.alergiesFromParse);




        // Take information from dashbord.
        Intent takeInf = getIntent();
        String objectID = takeInf.getStringExtra("objectID");
        Boolean isAuth = takeInf.getBooleanExtra("auth",false);

        if(!isAuth)
            editButton.setVisibility(View.INVISIBLE);

if(objectID.equals("3b33oyE3ik")){
    getAnimalInformation("1znBfD5R6W");
}else{
    getAnimalInformation(objectID);
}





    }

    public void getAnimalInformation(String objectID){

        // Get information from Parse Server
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Animals");
        query.getInBackground(objectID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if(e == null){
                    if(object != null){

                        final ParseFile animalImage = (ParseFile) object.get("image");
                        animalImage.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] data, ParseException e) {
                                if(e == null){
                                    if(data != null){
                                        bitmap = BitmapFactory.decodeByteArray(data,0,data.length);
                                        animalPic.setImageBitmap(bitmap);
                                    }else{
                                        System.out.println("There is an error but I don't know why.");
                                    }
                                }else{
                                    System.out.println("Error! "+e.getLocalizedMessage());
                                }
                            }
                        });

                        animalName.setText(object.getString("name"));
                        animalBasicInf.setText(object.getString("basicInf"));
                        animalVaccines.setText(object.getString("vaccines"));
                        animalAlergies.setText(object.getString("alergies"));

                        String idText = "#"+object.getObjectId();
                        animalID.setText(idText);


                    }else{
                        System.out.println("There is an error but i don't know why");
                    }
                }else{
                    e.printStackTrace();
                }
            }
        });
    }


    public void goBack(View view){
// Go back button
        // TODO Assign it also on hardware back button

            if(MainActivity.isLoggedIn){
                if(ParseUser.getCurrentUser().getUsername().equals("admin")){
                    Intent intent0 = new Intent(getApplicationContext(),AdminPageActivity.class);
                        startActivity(intent0);
                }else{
                    Intent intent2 = new Intent(getApplicationContext(),DashboardActivity.class);
                    startActivity(intent2);
                }

            }else{
                Intent intent1 = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent1);
            }




    }

    public void showQR(View view){
        Intent showQR = new Intent(getApplicationContext(),QrPrintActivity.class);
        // To remove the # sign
        String id =     animalID.getText().toString().substring(1,animalID.getText().length());
        showQR.putExtra("ID",id);
        startActivity(showQR);
    }



    //DID Burayı hallet adam edit logosuna tıklayınca değiştirme sayfasına gidecek.
    //orda değişiklik yapıldıktan sonra sayfaya dönecek...

    public void edit(View view){
        Intent hayvan = new Intent(getApplicationContext(),EditAnimalActivity.class);
        // Transform image to byte Array
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        // Do not try pass image directly, It doesn't work.
        // https://stackoverflow.com/questions/11010386/passing-android-bitmap-data-within-activity-using-intent-in-android
        hayvan.putExtra("image", byteArray); // Resim
        hayvan.putExtra("name",animalName.getText().toString());
        hayvan.putExtra("basicInf",animalBasicInf.getText().toString());
        hayvan.putExtra("vaccines",animalVaccines.getText().toString());
        hayvan.putExtra("alergies",animalAlergies.getText().toString());

        // To remove the # sign
        String id =     animalID.getText().toString().substring(1,animalID.getText().length());
        hayvan.putExtra("id",id);

        startActivity(hayvan);

    }

    public void add_lost(View view){
        Toast.makeText(getApplicationContext(),"Project currently in BETA version.",Toast.LENGTH_LONG).show();
    }


}
package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.msalihbindak.petqrv01.barcode.BarcodeCaptureActivity;

public class MainActivity extends AppCompatActivity {
    private String LOG_TAG = MainActivity.class.getSimpleName();
    private int BARCODE_READER_REQUEST_CODE = 1;
    TextView resultText;
    TextView textView;
    Button petOwnerLogin;
    Button scanQR;
    Button adminLogin;
    public static Boolean isLoggedIn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        petOwnerLogin = findViewById(R.id.petOwnerButton);
        scanQR = findViewById(R.id.scanQR);
        adminLogin = findViewById(R.id.adminLogin);


        textView = findViewById(R.id.textView);
        resultText = findViewById(R.id.resultText);
        resultText.setText("");



    }


    public void petOwnerLogin(View view) {

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra("logintype", "Pet Owner");
        startActivity(intent);
    }

    //com.varvet.barcodereadersample.MainActivity.Companion#getBARCODE_READER_REQUEST_CODE
    public void scanQR(View view) {
        Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
        //startActivity(intent);
        startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //String p = barcode.cornerPoints;
                    String result = barcode.displayValue;
                    resultText.setText("Loading...");
                    Intent intent = new Intent(getApplicationContext(),AnimalPageActivity.class);
                    intent.putExtra("auth",false); // Bu adam dışardan tarama yaptığı için yetkisi yok.
                    intent.putExtra("objectID",result);
                    startActivity(intent); //TODO This intent is not efficient. It will change
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

                } else {
                    String result = "No barcode captured";
                    resultText.setText(result);
                }
            } else {
                //Toast.makeText(getApplicationContext(),"Error reading barcode",Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG,String.format("Error reading barcode"));
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void adminLogin(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.putExtra("logintype", "Admin");
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }


    public void missing(View view){
        Toast.makeText(getApplicationContext(),"Project currently in BETA version.",Toast.LENGTH_LONG).show();
    }


}
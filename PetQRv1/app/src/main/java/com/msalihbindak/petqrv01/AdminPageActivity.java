package com.msalihbindak.petqrv01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.msalihbindak.petqrv01.barcode.BarcodeCaptureActivity;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class AdminPageActivity extends AppCompatActivity {
    private String LOG_TAG = AdminPageActivity.class.getSimpleName();
    private int BARCODE_READER_REQUEST_CODE = 1;

    TextView  welcomeAdmin;
    TextView resultText;
    EditText petIdText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);

        resultText = findViewById(R.id.resultText);

        welcomeAdmin = findViewById(R.id.welcomeAdmin);
        welcomeAdmin.setText("Welcome Admin.");

        petIdText = findViewById(R.id.petIdText);
    }



    public void findAnimal(View view){

        String petID= petIdText.getText().toString();

        Intent find = new Intent(getApplicationContext(),AnimalPageActivity.class);
        find.putExtra("auth",true);
        find.putExtra("objectID",petID);
        startActivity(find);
    }


    //DID QR KOdu taratma seçeneği
    public void scanQRcode(View view) {
        Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
        //startActivity(intent);
        startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    //String p = barcode.cornerPoints;
                    String result = barcode.displayValue;
                    // TODO control for string is an ID
                    resultText.setText("Loading...");
                    Intent intent = new Intent(getApplicationContext(),AnimalPageActivity.class);
                    intent.putExtra("auth",true);
                    intent.putExtra("objectID",result);
                    startActivity(intent); //TODO This intent is not efficient. It will change

                } else {
                    String result = "No barcode captured";
                    resultText.setText(result);
                }
            } else {
                //Toast.makeText(getApplicationContext(),"Error reading barcode",Toast.LENGTH_SHORT).show();
                Log.e(LOG_TAG,String.format("Error reading barcode"));
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }



    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.add_animal){
                    Intent addAnimal = new Intent(getApplicationContext(),AddAnimalActivity.class);
                    startActivity(addAnimal);
                }
                if(item.getItemId() == R.id.logout){
                    ParseUser.logOutInBackground(new LogOutCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e != null){
                                Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                            }else{
                                ParseUser.logOut();
                                Toast.makeText(getApplicationContext(),"We hope see you again!",Toast.LENGTH_LONG).show();
                                Intent home = new Intent(getApplicationContext(),MainActivity.class);
                                MainActivity.isLoggedIn = false;
                                startActivity(home);
                            }
                        }
                    });
                }

                return false;
            }
        });

    }
}

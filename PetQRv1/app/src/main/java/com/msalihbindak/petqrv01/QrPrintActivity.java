package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
 */
import android.support.v7.app.AppCompatActivity;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class QrPrintActivity extends AppCompatActivity {
    ImageView qrCode;
    TextView textView;
    Button shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_print);

        Intent intent = getIntent();
        String objectId;
        objectId = intent.getStringExtra("ID");


        qrCode = findViewById(R.id.qrCodeImage);
        textView = findViewById(R.id.textView);
        shareButton = findViewById(R.id.shareqrButton);
        String urlCreated = "https://chart.googleapis.com/chart?chs=500&cht=qr&chl="+objectId+"&choe=UTF-8";
        Picasso.get().load(urlCreated).into(qrCode);
        //https://stackoverflow.com/questions/2471935/how-to-load-an-imageview-by-url-in-android?rq=1
        String information = "Ready!";
        textView.setText(information);
    }

    // https://code.tutsplus.com/tutorials/android-sdk-implement-a-share-intent--mobile-8433

    public void shareButton(View view){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("image/png");
        //https://stackoverflow.com/questions/13065838/what-are-the-possible-intent-types-for-intent-settypetype
        //String shareBody = "PetQR";
        //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        //sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Select printer to print now or send anywhere for print later."));
    }

    public void goDashboard(View view){
        Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
        startActivity(intent);
    }

}

package com.msalihbindak.petqrv01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class DataLoadActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_load);
        ParseAnalytics.trackAppOpenedInBackground(getIntent()); // For parse


        Intent intent = getIntent();
        intent.getStringExtra("qrcode");

        // Creating fake animal for test getting data
        ParseObject object = new ParseObject("Animals");
        object.put("Name","Maya");  // Veri girme  put(KEY,VALUE) KEY -> COLUMN NAME , VALUE -> VALUE
        object.put("BasicInformation","maya is a English Cat");
        object.put("Vaccines","Çiçek Aşısı,Kuş Gribi Aşısı");
        object.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    Toast.makeText(getApplicationContext(),"Animal added successfuly",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });













    }
}

package com.msalihbindak.petqrv01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends AppCompatActivity {
    EditText usernameText;
    EditText passwordText;
    TextView login;
    Button   loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameText = findViewById(R.id.usernameText);
        passwordText = findViewById(R.id.passwordText);
        login = findViewById(R.id.login);
        loginButton = findViewById(R.id.loginButton);


        Intent intent = getIntent();
        String recieved = intent.getStringExtra("logintype");
        String loginText = recieved+" Login";

        // FOR DEMO
        if (recieved.equals("Admin")){
            usernameText.setText("admin",TextView.BufferType.EDITABLE);
            passwordText.setText("123456");
        }else{
            usernameText.setText("cybersalih1",TextView.BufferType.EDITABLE);
            passwordText.setText("54682597410");
        }


        login.setText(loginText);
    }



    public void login(View view) {

        ParseUser.logInInBackground(usernameText.getText().toString(), passwordText.getText().toString(), new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (e == null) {
                    if (user != null) {
                        Toast.makeText(getApplicationContext(), "Welcome " + user.getUsername(), Toast.LENGTH_SHORT).show();
                        MainActivity.isLoggedIn = true;
                        if(ParseUser.getCurrentUser().getUsername().equals("admin")){
                            Intent page = new Intent(getApplicationContext(), AdminPageActivity.class);
                            startActivity(page);
                        }else{
                            Intent page = new Intent(getApplicationContext(), DashboardActivity.class);
                            startActivity(page);
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "There is an error but i don't know why", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public void register(View view){
        Intent registerPage = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(registerPage);
    }




}

package com.msalihbindak.petqrv01;
/*
VGhpcyBhcHAgYmVsb25ncyB0byBNZWhtZXQgU2
FsaWggQmluZGFrIChtZWhtZXRzYWxpaGJpbmRha
0BnbWFpbC5jb20pLiBBbGwgb2YgcmVmZXJlbmNlcy
Bjb21tZW50ZWQgd2hlcmUgdGhlIGNvZGUgdXNlZC4=
*/

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

public class AddAnimalActivity extends AppCompatActivity {

    private int CHOOSE_IMAGE_REQUEST_CODE = 2;
    private int PERMISSION_REQUEST_CODE = 1;

    ImageView animalImage;
    Bitmap choosenAnimalImage;
    EditText animalName;
    EditText animalBasicInf;
    EditText animalVaccines;
    EditText animalAlergies;
    ParseUser petOwner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_animal);
        animalImage = findViewById(R.id.animalPic);
        animalName = findViewById(R.id.animalName);
        animalBasicInf = findViewById(R.id.animalBasic);
        animalVaccines = findViewById(R.id.animalVaccines);
        animalAlergies = findViewById(R.id.animalAlergies);
        petOwner = ParseUser.getCurrentUser();
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void chooseImage(View view){
        // Check permissions if ok go to take picture
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
            Toast.makeText(getApplication(),"You will go to take picture...",Toast.LENGTH_LONG).show();
        }else{
            // Go to take request...
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSION_REQUEST_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // Eleman izin vermiş resmi çekmeye gidiyorum.
                Intent chooseImage = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(chooseImage,CHOOSE_IMAGE_REQUEST_CODE);
                // Choose Image içindeki kodun aynısı çünkü orada da izin varsa yapacağım şeyleri şimdi izin aldıktan sonra yapıyorum.
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHOOSE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            //izin isteyip OK cevabı döndüğünü ve resmi galeriden çekebildiğimi teyit ettim.
            Uri selectedImage = data.getData();
            try{
                choosenAnimalImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                animalImage.setImageBitmap(choosenAnimalImage);
            }catch (Exception e){
                // e.printStackTrace(); // For debugging
                Toast.makeText(getApplication(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void addAnimal(View view){
        // Check name, and basic information is full


            String aName = animalName.getText().toString();
            String aBasicInf = animalBasicInf.getText().toString();
            String aVaccines = animalVaccines.getText().toString();
            String aAlergies = animalAlergies.getText().toString();

            // I tried the aName == null  it does not work. Do not try!


            // IMAGE CONVERTS TO PARSE OBJECT START
            ByteArrayOutputStream byteArrayOfAnimalImage = new ByteArrayOutputStream();
            choosenAnimalImage.compress(Bitmap.CompressFormat.PNG,20,byteArrayOfAnimalImage);
            byte[] imageBytes = byteArrayOfAnimalImage.toByteArray();
            ParseFile parseAnimalImage = new ParseFile("animal.png",imageBytes);
            // END

        if(aName.isEmpty() || aBasicInf.isEmpty()){
            Toast.makeText(getApplication(),"Please fill at least Name and Basic Information",Toast.LENGTH_SHORT).show();
        }else {

            final ParseObject object = new ParseObject("Animals");

            object.put("image",parseAnimalImage);
            object.put("ownerName",petOwner.getUsername());
            object.put("name",aName);
            object.put("basicInf",aBasicInf);
            object.put("vaccines",aVaccines);
            object.put("alergies",aAlergies);

            object.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null){
                        Toast.makeText(getApplicationContext(),e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(),"Animal Successfully Added",Toast.LENGTH_SHORT).show();
                        Intent qr = new Intent(getApplicationContext(),QrPrintActivity.class);
                        if(object.getObjectId() != null){
                            qr.putExtra("ID",object.getObjectId());
                        }else{
                            Toast.makeText(getApplicationContext(),"I don't know whats going on.",Toast.LENGTH_LONG).show();
                        }
                        startActivity(qr);
                    }

                }
            });

            }

    }

}


